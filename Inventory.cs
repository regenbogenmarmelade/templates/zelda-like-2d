using Godot;
using PropertyChanged;

[AddINotifyPropertyChangedInterface]
public class Inventory : Node
{
    // let Godot Autoload manage the singleton instance
    private static ulong _instanceId;

    [DoNotNotify] public static Inventory Instance => GD.InstanceFromId(_instanceId) as Inventory;

    public override void _Ready()
    {
        _instanceId = GetInstanceId();
    }

    [Signal]
    public delegate void InventoryChanged(Inventory inventory);

    [Export]
    [OnChangedMethod(nameof(EmitInventoryChanged))]
    public int NumItems { get; set; }

    private void EmitInventoryChanged()
    {
        EmitSignal(nameof(InventoryChanged), this);
    }
}
