using Godot;

public class Main : Control
{
    private void start_game()
    {
        var hud = ResourceLoader.Load<PackedScene>("res://ui/HUD.tscn").Instance<HUD>();
        GetTree().Root.AddChild(hud);
        GetTree().ChangeScene("maps/Map1.tscn");
    }
}
