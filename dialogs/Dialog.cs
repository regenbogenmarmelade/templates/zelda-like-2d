using Godot;
using Ink;
using Ink.Runtime;

public class Dialog : Panel
{
    private Story _story;

    public override void _Ready()
    {
        var storyFile = new File();
        storyFile.Open("res://dialogs/dummy.ink", File.ModeFlags.Read);
        try
        {
            var compiler = new Compiler(storyFile.GetAsText());
            _story = compiler.Compile();
        }
        finally
        {
            storyFile.Close();
        }

        continue_story();
    }

    private void continue_story()
    {
        if (!_story.canContinue)
        {
            QueueFree();
            return;
        }

        var label = GetNode<Label>("Label");
        label.Text = _story.Continue();
    }
}
