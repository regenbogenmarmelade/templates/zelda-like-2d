using Godot;

public class Item : Node2D
{
    private void area_entered(Area2D area)
    {
        if (!Player.IsParentOf(area)) return;
        
        Inventory.Instance.NumItems++;
        QueueFree();
    }
}