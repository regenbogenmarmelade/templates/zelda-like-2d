using Godot;

public class Door : Node2D
{
    [Export] public string Target;

    private void area_entered(Area2D area)
    {
        if (Player.IsParentOf(area))
        {
            GetTree().ChangeScene($"maps/{Target}.tscn");
        }
    }
}
