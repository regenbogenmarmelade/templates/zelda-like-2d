using System.Collections.Generic;
using System.Linq;
using Godot;

public class Player : Node2D
{
    private readonly Dictionary<string, Vector2> _movementVectors = new Dictionary<string, Vector2>
    {
        { "ui_down", Vector2.Down },
        { "ui_left", Vector2.Left },
        { "ui_right", Vector2.Right },
        { "ui_up", Vector2.Up },
    };

    public override void _UnhandledKeyInput(InputEventKey @event)
    {
        if (!@event.Pressed)
        {
            return;
        }

        var movement = _movementVectors
            .Where(pair => @event.IsAction(pair.Key))
            .Select(pair => pair.Value)
            .DefaultIfEmpty(Vector2.Zero)
            .First();
        var size = GetNode<ColorRect>("Dummy").RectSize;

        Translate(movement * size);
    }

    public static bool IsParentOf(Area2D area)
    {
        return area.GetParent() is Player;
    }
}