using Godot;

public class HUD : CanvasLayer
{
    public HUD()
    {
        Name = nameof(HUD);
    }

    public override void _Ready()
    {
        Inventory.Instance.Connect(nameof(Inventory.InventoryChanged), this, nameof(InventoryChanged));
        
        InventoryChanged(Inventory.Instance);
    }

    private void InventoryChanged(Inventory inventory)
    {
        var label = GetNode<Label>("NumItems");
        label.Text = inventory.NumItems.ToString();
    }
}
